#!/usr/bin/python3
import random
from faker import Faker

class create_info:
    namepath='dicts/name.txt'
    lastnamepath='dicts/lname.txt'
    name=''
    lname=''
    mail=''
    bday=''
    address=''
    marital_status=''
    ssn=''
    domain= random.choice(["hotmail.com", "gmail.com","outlook.com", "yahoo.com"])

    def __init__(self,name='',lname='',domain=''):
        if(name==''):
            self.name=self.get_name()
        else:
            self.name=name
        if (lname==''):
            self.lname=self.get_lname()
        else:
            self.lname=lname
        if(domain!=''):
            self.domain=domain

        self.number= self.get_number()
        self.mail=self.get_email()
        self.bday=self.get_bdate()
        fake = Faker()
        self.address=fake.address().replace('\n',' ')
        self.ssn=fake.ssn().replace('\n',' ')
        options=['married','single','divorced','widow*']
        self.marital_status=random.choice(options)

    def get_number(self):
        ph_no = []

        # the first number should be in the range of 6 to 9
        ph_no.append(random.randint(6, 9))

        # the for loop is used to append the other 9 numbers.
        # the other 9 numbers can be in the range of 0 to 9.
        for i in range(1, 10):
            ph_no.append(random.randint(0, 9))

        # printing the number
        tmp = [str(int) for int in ph_no]
        num = "". join(tmp)
        return num

    def get_value(self):
        return "{} | {} | {} | {} | {} | {} | {} | {}".format(self.name,self.lname,self.bday,self.mail,self.number,self.address,self.marital_status,self.ssn)

    def get_email(self,first_name='',last_name='',domain='',n=1):
        val=[]
        for x in range(0,n):
            if(domain==''):
                domain=self.domain
            if(first_name==''):
                first_name=self.name
            if(last_name==''):
                last_name=self.lname
            x=random.randrange(6)

            if x==0:
                #firsnamelastname@domain
                val.append(str(first_name+last_name+'@'+domain))
            if x==1:
                #firsname.lastname@domain
                val.append(str(first_name+"."+last_name+'@'+domain))
            if x==2:
                #firsname.lastname@domain
                val.append(str(first_name+"-"+last_name+'@'+domain))
            if x==3:
                #flastname@domain
                val.append(str(first_name[0]+last_name+'@'+domain))
            if x==4:
                #firsnameRandomNr@domain
                number=random.randrange(10000)
                val.append(str(first_name+str(number)+'@'+domain))
            if x==5:
                #firsnameRandomNr@domain
                number=random.randrange(2020)
                val.append(str(first_name+str(number)+last_name+'@'+domain))
        if(n==1):
            return val[0]
        else:
            return val

    def get_name(self,n=1):
        tmp=[]
        with open(self.namepath) as my_file:
            for line in my_file:
                tmp.append(line)
        if(n==1):
            return random.choice(tmp).rstrip().lower()
        else:
            val=[]
            for x in range(0,n):
                val.append(random.choice(tmp).rstrip().lower())
            return val

    def get_lname(self,n=1):
        tmp=[]
        with open(self.lastnamepath) as my_file:
            for line in my_file:
                tmp.append(line)
        if(n==1):
            return random.choice(tmp).rstrip().lower()
        else:
            val=[]
            for x in range(0,n):
                val.append(random.choice(tmp).rstrip().lower())
            return val

    def get_bdate(self,min_year=1964, max_year=2010):
        import random
        from datetime import datetime, timedelta

        # generate a datetime in format yyyy-mm-dd hh:mm:ss.000000
        start = datetime(min_year, 1, 1, 00, 00, 00)
        years = max_year - min_year + 1
        end = start + timedelta(days=365 * years)
        data=str(start + (end - start) * random.random()).split(' ')[0].split('-')
        return data[2]+'-'+data[1]+'-'+data[0]
